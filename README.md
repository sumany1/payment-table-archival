# README #

Scripts (pgbench/db) for Payments partitioned table perf analysis . Scripts for backfill , replication trigger etc for the cutover.

## Details

-  Perf DB instance - aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com
-  User creation pipeline - https://jenkins.internal.perf1.zetaapps.in/job/self_service_postgresql_user_creation/
-  Findings - https://docs.google.com/document/d/1dte_wJ_RuQfi3H4wn95ytyV9P4LyT_qY23ENzJiVQXI/edit?usp=sharing 