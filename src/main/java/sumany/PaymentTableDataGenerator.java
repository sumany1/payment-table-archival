package sumany;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public class PaymentTableDataGenerator {
	
	
	/*
	 *  -- $$ID_PK$$ 403874077613
 -- $$PREQID_UK2$$ "092010001052421_92111227_201706_000069220920_210927232600"
 -- $$PREQFRM_UK2$$ "198636:1:1"
 -- $$PID_UK1$$ 3b518566-e82d-4064-a19b-46b0ab8c7e36
 -- $$PAUTHSIGN_UK1$$ 1272871@zetauser.zeta.in/wallet_auto_authorization
 
 -- $$BID_IDX1$$ 233413
 -- $$SID_IDX1$$ 1
 
 -- $$ISFIN_IDX2$$ TRUE
 
 -- $$IFI_IDX3$$ 163925
 
 -- $$RID_IDX4$$ f93ef7a4-b77d-4816-87b3-741e2963cf6b
  
 -- $$TCID_IDX5$$ 784387
  
 -- $$PYRUID_IDX6$$ 6057359

 -- $$CRTDT_IDX7$$ 2021-09-27 17:55:52.193801+00
	 */
	
	AtomicLong idL = new AtomicLong();
	Random idRnd = new Random();
	Random preqRnd = new Random();
	String preqid_suffix = "_92111227_201706_000069220920_210927232600";
	String preqFrm_suffix = ":1:1";
	String pauthsign_suffix = "@zetauser.zeta.in/wallet_auto_authorization";
	String qryTemplate;
	
	DateFormat df ;
	
	public PaymentTableDataGenerator(String qryTemplate) {

		this.qryTemplate = qryTemplate;
		 df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
	}
	
	public static void main(String[] args) throws IOException {

		
		
		
		
		//Date dt = new Date();
		//dt.
		
		
		try( BufferedReader tmpltFileRdr =  new BufferedReader(new InputStreamReader( PaymentTableDataGenerator.class.getClassLoader().getResourceAsStream("payments_insert-template.sql")))){
			
			PaymentTableDataGenerator pgen = new PaymentTableDataGenerator(tmpltFileRdr.readLine());
			
			
			ArrayList<InsertQueryHolder> ress = new ArrayList<>();
			//loop here
	        //new DataTime()
			//Calendar.getInstance().;
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			cal.set(2021,8,1,0,0,0);
			cal2.set(2021,8,1,0,0,0);
			//cal2.set(2022,0,1,0,0,0);
			cal2.add(Calendar.MONTH, 3);
			
			while(cal.before(cal2)) {
				InsertQueryHolder res = pgen.generateInsertQuery(cal.getTime());
				
				ress.add(res);
				cal.add(Calendar.SECOND, 86); // 1000 rows per day
				//System.out.println(res);
			}
			
			System.out.println("num of rows generated -> "+ress.size());
			try(PrintWriter insertsFl = new PrintWriter(new FileWriter("generated-queries/payments_inserts.sql",true),true)){
				try(PrintWriter insertParamsFl = new PrintWriter(new FileWriter("generated-queries/payments_inserts_params.csv",true),true)){
				//insertsFl.append(res);
				for(InsertQueryHolder res1:ress) {
					
					insertsFl.println(res1.qry);
					insertParamsFl.println(res1.params.toString());
				}
			}
			}
			
		}
	}

	private static class InsertQueryHolder{
		public String qry;
		public InsertQueryParams params;
	}
	
	private static class InsertQueryParams{
		public String id ;
		public String createdAt;
		public String preqid;
		public String preqFrm;
		public String pid;
		public String pauthSign;
		public String bid;
		public String sid;
		public String isFin;
		public String ifi;
		public String rid;
		public String tcid;
		public String pyruid;
		@Override
		public String toString() {
			return  id + "," + createdAt + "," + preqid + ","
					+ preqFrm + "," + pid + "," + pauthSign + "," + bid + "," + sid
					+ "," + isFin + "," + ifi + "," + rid + "," + tcid + "," + pyruid;
		}
		
		
	}
	private  InsertQueryHolder generateInsertQuery( Date dt) {
		
		InsertQueryParams params = new InsertQueryParams();
		
		params.id = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE)+""; //id
		 params.createdAt = df.format(dt); //String createdAt		
        
		//String id = idL.incrementAndGet()+"";
		params.preqid = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE)+preqid_suffix; //String preqid = 
		
		params.preqFrm =   preqRnd.nextInt(Integer.MAX_VALUE)+preqFrm_suffix;//String preqFrm =
		params.pid  =  UUID.randomUUID().toString(); //String pid =
		params.pauthSign = preqRnd.nextInt(Integer.MAX_VALUE)+pauthsign_suffix; //String pauthSign = 
		params.bid = preqRnd.nextInt(Integer.MAX_VALUE)+"";
		params.sid = preqRnd.nextInt(Integer.MAX_VALUE)+"";
		params.isFin = Boolean.valueOf(preqRnd.nextBoolean()).toString();
		params.ifi = preqRnd.nextInt(Integer.MAX_VALUE)+"";
		params.rid = UUID.randomUUID().toString();
		params.tcid = preqRnd.nextInt(Integer.MAX_VALUE)+"";
		params.pyruid = preqRnd.nextInt(Integer.MAX_VALUE)+"";
		
		
		String res  = qryTemplate.replace("$$ID_PK$$",params.id )
				                 .replace("$$PREQID_UK2$$", params.preqid)
				                 .replace("$$PREQFRM_UK2$$", params.preqFrm)
				                 .replace("$$PID_UK1$$", params.pid)
				                 .replace("$$PAUTHSIGN_UK1$$", params.pauthSign)
				                 .replace("$$BID_IDX1$$", params.bid)
				                 .replace("$$BID_IDX1$$", params.bid)
				                 .replace("$$SID_IDX1$$", params.sid)
				                 .replace("$$ISFIN_IDX2$$", params.isFin)
				                 .replace("$$IFI_IDX3$$", params.ifi)
				                 .replace("$$RID_IDX4$$", params.rid)
				                 .replace("$$TCID_IDX5$$", params.tcid)
				                 .replace("$$PYRUID_IDX6$$", params.pyruid)
				                 .replace("$$CRTDT_IDX7$$", params.createdAt)
				                 ;
		InsertQueryHolder holder = new InsertQueryHolder();
		holder.qry = res;
		holder.params = params;
		return holder;
	}

}
