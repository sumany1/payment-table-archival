CREATE EXTENSION dblink;
CREATE EXTENSION postgres_fdw;

SELECT dblink_connect('host= user= password= dbname=payment');
drop user mapping for  master_preprod server payment_dblink;
drop user mapping for  postgres server payment_dblink;
--drop server payment_dblink;
CREATE SERVER payment_dblink FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '', dbname 'payment', port '5432');

create user master_preprod;

CREATE USER MAPPING FOR master_preprod SERVER payment_dblink OPTIONS ( "user" '', password '') ;

CREATE USER MAPPING FOR postgres SERVER payment_dblink OPTIONS ( "user" '', password '') ;

SELECT dblink_connect('payment_dblink');

--TODO: parameterize from Date ( hard-coded here '2021-10-01' )

-- SELECT string_agg(a.attname || ' ' || pg_catalog.format_type(a.atttypid, a.atttypmod), ',')
-- FROM pg_catalog.pg_attribute a
-- JOIN pg_class c
-- ON c.oid = a.attrelid
-- WHERE c.relname = 'auth_data' AND a.attnum > 0 AND NOT a.attisdropped ;

--select count(*) from public.incomplete_payment;

--delete from public.incomplete_payment;

insert into payments select * from dblink ('payment_dblink', $$select * from public.payments where createdat >='2021-10-01' and createdat <'2021-10-02'$$) 
payment_dblink_pp ( id bigint,paymentrequestid character varying(256),paymentrequestfrom character varying(128),paymentid character varying(256),paymentauthorizationsignatory character varying(128),state payment_state,statetransitions jsonb,requestchanneltype character varying(32),paymentrequest jsonb,paymentauthorization jsonb,attributes jsonb,payer jsonb,payee jsonb,coupondebitauths jsonb,transactionreceipt jsonb,authorizationreceipt jsonb,capturerequest jsonb,capturereceipt jsonb,paymentreceipt jsonb,reversalrequest jsonb,reversaltransactionreceipt jsonb,reversalreceipt jsonb,transactioncode character varying(32),createdat timestamp with time zone,isfinished boolean,ifi bigint,resourceid character varying(256),paymentcode text,channelcode text,paymentplan jsonb );

insert into abuses select * from dblink ('payment_dblink',$$ select *  from public.abuses where attemptedat >='2021-10-01' and attemptedat <'2021-10-02' $$) payment_dblink_pp (requester text,abusetype text,attemptedat timestamp with time zone,address text,payer text);

insert into block_status select * from dblink ('payment_dblink',$$ select * from public.block_status $$)
payment_dblink_pp (requester text,abusetype text,failedattempts integer,blockeduntil timestamp with time zone);

insert into block_limits select * from dblink ('payment_dblink',$$ select * from public.block_limits; $$)
payment_dblink_pp (abusetype text,maxfailedattempts integer,blockmins integer);

insert into incomplete_payment select * from dblink ('payment_dblink',$$ select incomplete_payment.id, payments.createdat
from public.incomplete_payment inner join public.payments 
on incomplete_payment.id= payments.id
where payments.createdat >='2021-10-01' and payments.createdat <'2021-10-02'; $$)
payment_dblink_pp (id bigint,created_at timestamp with time zone);

insert into requester_whitelist select * from dblink ('payment_dblink',$$ select * from public.requester_whitelist $$)
payment_dblink_pp (jid text);

-- TODO: check reversal createdat
insert into reversal_transactions select reversalid, id, state, reversalrequest, reversaltransactionreceipt, reversalreceipt, createdat,  modifiedat,ifi from dblink ('payment_dblink',$$ select reversal_transactions.reversalid, reversal_transactions.id, reversal_transactions.state, reversal_transactions.reversalrequest, reversal_transactions.reversaltransactionreceipt, reversal_transactions.reversalreceipt, reversal_transactions.createdat, reversal_transactions.ifi, reversal_transactions.modifiedat 
from public.reversal_transactions inner join public.payments 
on reversal_transactions.id= payments.id
where payments.createdat >='2021-10-01' and payments.createdat <'2021-10-02'; $$)
payment_dblink_pp (reversalid text,id bigint,state reversal_state,reversalrequest jsonb,reversaltransactionreceipt jsonb,reversalreceipt jsonb,createdat timestamp with time zone,ifi bigint,modifiedat timestamp with time zone);

insert into used_auths select * from dblink ('payment_dblink',$$ SELECT used_auths.* FROM public.used_auths INNER JOIN payments ON used_auths.id = payments.id WHERE payments.createdat >='2021-10-01' and payments.createdat <'2021-10-02'; $$)
payment_dblink_pp (authid character varying(256),id bigint);

insert into auth_data select * from dblink ('payment_dblink',$$ select * from public.auth_data $$)
payment_dblink_pp (auth_id text,auth jsonb,payments_id bigint,state auth_data_state,createdat timestamp with time zone);
