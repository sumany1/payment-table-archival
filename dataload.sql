-- useful script samples used in dataload 

SELECT cron.schedule('payment_perf_datagen_09_1M', '*/30 * * * *', $$ call payment_perf_datagen(1000000,'09')$$);

SELECT cron.schedule('payments_old2_p10_44M_load', '* * */2 * *', $$ insert into payments_old2 (select * from payments_p2021_09 where id>44000000 and id<54000000) on conflict do nothing$$);