CREATE FUNCTION public.handle_partition_payment_insert()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
 
 insert into public.payments_old1 (id,paymentrequestid,paymentrequestfrom,paymentid,paymentauthorizationsignatory,state,statetransitions,requestchanneltype,paymentrequest,paymentauthorization,attributes,payer,payee,coupondebitauths,transactionreceipt,authorizationreceipt,capturerequest,capturereceipt,paymentreceipt,reversalrequest,reversaltransactionreceipt,reversalreceipt,transactioncode,createdat,isfinished,ifi,resourceid,paymentcode,channelcode,paymentplan) 
 values(NEW.id,NEW.paymentrequestid,NEW.paymentrequestfrom,NEW.paymentid,NEW.paymentauthorizationsignatory,NEW.state,NEW.statetransitions,NEW.requestchanneltype,NEW.paymentrequest,NEW.paymentauthorization,NEW.attributes,NEW.payer,NEW.payee,NEW.coupondebitauths,NEW.transactionreceipt,NEW.authorizationreceipt,NEW.capturerequest,NEW.capturereceipt,NEW.paymentreceipt,NEW.reversalrequest,NEW.reversaltransactionreceipt,NEW.reversalreceipt,NEW.transactioncode,NEW.createdat,NEW.isfinished,NEW.ifi,NEW.resourceid,NEW.paymentcode,NEW.channelcode,NEW.paymentplan); 

RETURN NEW;
END;
$BODY$;
