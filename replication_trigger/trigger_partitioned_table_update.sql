CREATE TRIGGER trigger_partition_payment_update
AFTER UPDATE  ON public.payments_p2021_11
FOR EACH ROW EXECUTE FUNCTION public.handle_partition_payment_update();