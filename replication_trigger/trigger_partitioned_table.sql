CREATE TRIGGER trigger_partition_payment_insert
AFTER INSERT  ON public.payments_p2021_11
FOR EACH ROW EXECUTE FUNCTION public.handle_partition_payment_insert();