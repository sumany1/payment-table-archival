CREATE FUNCTION public.handle_partition_payment_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
 
 update  public.payments_old1  set paymentrequestid=NEW.paymentrequestid, paymentrequestfrom =NEW.paymentrequestfrom, paymentid=NEW.paymentid,paymentauthorizationsignatory=NEW.paymentauthorizationsignatory,state=NEW.state,statetransitions=NEW.statetransitions,requestchanneltype=NEW.requestchanneltype,paymentrequest=NEW.paymentrequest,paymentauthorization=NEW.paymentauthorization,attributes=NEW.attributes,payer=NEW.payer,payee=NEW.payee,coupondebitauths=NEW.coupondebitauths,transactionreceipt=NEW.transactionreceipt,authorizationreceipt=NEW.authorizationreceipt,capturerequest=NEW.capturerequest,capturereceipt=NEW.capturereceipt,paymentreceipt=NEW.paymentreceipt,reversalrequest=NEW.reversalrequest,reversaltransactionreceipt=NEW.reversaltransactionreceipt,reversalreceipt=NEW.reversalreceipt,transactioncode=NEW.transactioncode,createdat=NEW.createdat,isfinished=NEW.isfinished,ifi=NEW.ifi,resourceid=NEW.resourceid,paymentcode=NEW.paymentcode,channelcode=NEW.channelcode,paymentplan=NEW.paymentplan 
 where id = NEW.id ;  

RETURN NEW;
END;
$BODY$;
