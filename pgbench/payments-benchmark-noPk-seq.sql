--\set tmpid random(1,91424)
\set b 0
BEGIN;
-- pick a random row - r1
-- csv in value in table , 
-- select r1 from the csv-table where id = <random-number> ( 0- 91k) // disk-read

\set p11offst  case when :a%3 = 0 then (:p11offst+1) else :p11offst end
\set p10offst  case when :a%2 = 0 then (:p10offst+1) else :p10offst end
\set p09offst  case when NOT( (:a%2 = 0) OR (:a%3 = 0))  then (:p09offst+1) else :p09offst end
--\set a (:client_id*10)
\set b :p09offst
\set b  case when :a%3 = 0 then :p11offst else :b end
\set b  case when :a%2 = 0 then :p10offst else :b end

  SELECT *   FROM public.tmp_pgbench_data1 where tmpid=:b \gset r1_

 --insert into  tmp1(a) values(:a); 
  

	SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel 
	 FROM public.payments WHERE id = :r1_id;
	 
    SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel 
     FROM public.payments WHERE paymentrequestid = ':r1_preqid' AND paymentrequestfrom = ':r1_preqfrm';
     
     
      SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel 
       FROM payments WHERE paymentid = ':r1_pid' AND paymentauthorizationsignatory = ':r1_pauthsign';
     
\set a :a+1

END;