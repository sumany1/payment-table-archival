--\set tmpid random(1,91424)
\set b 0
\set st ((:client_id*1912222) +:offset)
BEGIN;
-- pick a random row - r1
-- csv in value in table , 
-- select r1 from the csv-table where id = <random-number> ( 0- 91k) // disk-read

\set a case when :a = 0 then (:st +1) else ((:a+1)%:st + :st) end


  SELECT *   FROM public.tmp_pgbench_data1 where tmpid=:a \gset r1_

 --insert into  tmp1(a) values(:a); 
  

	SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments_old2.id) AS paymentChannel 
	 FROM public.payments_old2 WHERE id = :r1_id;
	 
    SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments_old2.id) AS paymentChannel 
     FROM public.payments_old2 WHERE paymentrequestid = ':r1_preqid' AND paymentrequestfrom = ':r1_preqfrm';
     
     
      SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments_old2.id) AS paymentChannel 
       FROM public.payments_old2 WHERE paymentid = ':r1_pid' AND paymentauthorizationsignatory = ':r1_pauthsign';
     

END;