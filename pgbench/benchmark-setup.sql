create table public.tmp_pgbench_data1(
	tmpid bigserial primary key , 
	id  bigint ,
	createdAt character varying(256) ,
	preqid character varying(256),
	preqFrm character varying(256),
	pid character varying(256),
	pauthSign character varying(256),
	bid character varying(256),
	sid character varying(256),
	isFin character varying(256),
	ifi character varying(256),
	rid character varying(256),
	tcid character varying(256),
	pyruid character varying(256));
	
	
	create table public.tmp_pgbench_data(
	tmpid bigserial primary key , 
	id  bigint ,
	createdAt text ,
	preqid text,
	preqFrm text,
	pid text,
	pauthSign text );
	
	create table public.tmp_pgbench_data1(
	tmpid bigserial primary key , 
	id  bigint ,
	createdAt text ,
	preqid text,
	preqFrm text,
	pid text,
	pauthSign text );
	
	insert into table public.tmp_pgbench_data1(	id ,createdAt ,preqid ,preqFrm ,pid ,pauthSign)  select id ,createdAt ,preqid ,preqFrm ,pid ,pauthSign from  public.tmp_pgbench_data;
	
-- 	 6612487
-- (1 row)
-- 
-- payment=> select max(tmpid) from public.tmp_pgbench_data1 where createdat >= '2021-09-01' and createdat <= '2021-10-01';
--     
--   6612487 - 178712486
--   
--   10-11
--   178712487
--   
--   11-12
--   350712487
  
	
	-- in psql run the following ( update the file location accordingly ) 
	
-- 	\copy  public.tmp_pgbench_data
-- from '/Users/sy/projects/payment-table-archival/generated-queries/payments_inserts_params.csv'
-- WITH (FORMAT 'csv' , HEADER true);

--alter table public.tmp_pgbench_data add column tmpid bigserial primary key;

-- pgbench -h localhost -p 5432 -U postgres -f pgbench/payments-benchmark-Pk.sql payment -r  -j 8 -c 8 -T 3600 -P 60


-- for sequential test
--  pgbench -h localhost -p 5432 -U postgres -f pgbench/payments-benchmark-Pk-seq.sql payment -r  -j 8 -c 8  -t 11425 -s 11425 -D a=0



--pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 16 -c 16  -t 1000 -s 19506250 -D a=0 -D ofst=2623948 >> res_noPk_1.txt &

 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 16 -c 16  -t 19506250 -s 19506250 -D a=0 -D ofst=2623948 >> res_noPk_3.txt &
 
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 900 -c 900  -t 346777 -s 346777 -D a=0 -D ofst=2623948 >> res_noPk_4.txt &
 
-- nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -j 900 -c 900  -t 346777 -s 346777 -D a=0 -D ofst=2623948 >> res_noPk_6.txt &
 
--  total rows=312100000
--   per pod = 104033100
--   per pod connections = 300
--   per pod iterations = 346777
--   
--   pod 1 offst = 2623948
--   pod  2 offst = 106657048
--   pod 3 offst = 210690148 338032
 
 --  very low throughput 
 -- 19-Nov 17:55 IST
 --pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -j 300 -c 300  -t 346777 -s 346777 -D a=0 -D ofst=2623948 >> res_Pk_1.txt &
 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -j 300 -c 300  -t 346777 -s 346777 -D a=0 -D ofst=106657048 >> res_Pk_1.txt &

--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -j 300 -c 300  -t 338032 -s 338032 -D a=0 -D ofst=210690148 >> res_Pk_1.txt &


-- PARTITION KEY #2

		--  total rows=312100000
		--   per pod = 104033100
		--   per pod connections = 90
		--   per pod iterations = 300000
		--   
		--   pod 1 offst = 2623948
		--   pod  2 offst = 29623948
		--   pod 3 offst = 56623948 338032
		-- 

--pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -c 90  -t 12000 -s 12000 -D a=0 -D ofst=2623948  -P 300 >> res_Pk_2.txt &
 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r   -c 90   -t 12000 -s 12000 -D a=0 -D ofst=29623948 -P 300>> res_Pk_2.txt &

--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r -c 90  -t 12000 -s 12000 -D a=0 -D ofst=56623948 -P 300 >> res_Pk_2.txt &



-- NO PARTITION KEY #2

		--  total rows=312100000
		--   per pod = 104033100
		--   per pod connections = 90
		--   per pod iterations = 300000
		--   
		--   pod 1 offst = 2623948
		--   pod  2 offst = 29623948
		--   pod 3 offst = 56623948 338032


--pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -c 90  -t 12000 -s 12000 -D a=0 -D ofst=2623948  -P 300 >> res_noPk_2.txt &
 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r   -c 90   -t 12000 -s 12000 -D a=0 -D ofst=29623948 -P 300>> res_noPk_2.txt &

--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r -c 90  -t 12000 -s 12000 -D a=0 -D ofst=56623948 -P 300 >> res_noPk_2.txt &


-- NO PARTITION KEY #3

		--  total rows=312100000
		--   per pod = 104033100
		--   per pod connections = 90
		--   per pod iterations = 300000
		--   
		--   pod 1 offst = 2623948
		--   pod  2 offst = 29623948
		--   pod 3 offst = 56623948 338032
		
	--	9-10
	--	6612487 - 178712486
  
  --10-11
  --178712487
  
  --11-12
  --350712487


-- +20M
--pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -c 90  -T 300  -D a=0 -D p09offst=26612487 -D p10offst=198712487  -D p11offst=370712487 -P 300 >> res_noPk_2.txt &
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -c 90  -t 12000 -s 12000  -D a=0 -D p09offst=26612487 -D p10offst=198712487  -D p11offst=370712487 -P 300 >> res_noPk_3.txt &

-- +60M 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r   -c 90   -t 12000 -s 12000 -D a=0 -D p09offst=66612487 -D p10offst=238712487  -D p11offst=410712487 -P 300>> res_noPk_3.txt &

-- +100M
--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r -c 90  -t 12000 -s 12000 -D a=0 -D p09offst=106612487 -D p10offst=278712487  -D p11offst=450712487 -P 300 >> res_noPk_3.txt &

--partition key

-- +140M
--pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -c 90  -T 300  -D a=0 -D p09offst=146612487 -D p10offst=318712487  -D p11offst=490712487 -P 300 >> res_Pk_2.txt &
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r  -c 90  -t 12000 -s 12000  -D a=0 -D p09offst=146612487 -D p10offst=318712487  -D p11offst=490712487 -P 300 >> res_Pk_3.txt &

-- +150M 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r   -c 90   -t 12000 -s 12000 -D a=0 -D p09offst=156612487 -D p10offst=328712487  -D p11offst=150000000 -P 300>> res_Pk_3.txt &

-- +169M
--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-Pk-seq.sql payment -r -c 90  -t 12000 -s 12000 -D a=0 -D p09offst=166612487 -D p10offst=338712487  -D p11offst=510712487 -P 300 >> res_Pk_3.txt &



-- NO PARTITION KEY

-- 20-Nov 1402 IST


 --pgbench-74489fcbbf-7wdv2
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 300 -c 300  -t 346777 -s 346777 -D a=0 -D ofst=2623948 -P300 >> res_noPk_1.txt &
 
 --pgbench-74489fcbbf-899z9
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 300 -c 300  -t 346777 -s 346777 -D a=0 -D ofst=106657048 -P300 >> res_noPk_1.txt &

--pgbench-74489fcbbf-jmzwd
 --nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-noPk-seq.sql payment -r  -j 300 -c 300  -t 338032 -s 338032 -D a=0 -D ofst=210690148 -P300 >> res_noPk_1.txt &


-- UN-PARTITIONED TABLE

--nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/non-part-payments-benchmark-noPk-seq.sql payment -r  -c 90  -t 12000 -s 12000  -D a=0 -D offset=661248  -P 300 >> res_non_part_1.txt &



-- FOR insert benchmarking use 

--  pgbench -h localhost -p 5432 -U postgres -f pgbench/payments-benchmark-inserts.sql -T 1800   -r  -j 8 -c 8 -P 60 payment


--nohup pgbench -h aws-perf-staging-mumbai-athena-m1.cvelqg13i41e.ap-south-1.rds.amazonaws.com -p 5432 -U sumany -f /tmp/payments-benchmark-inserts.sql payment -r   -c 90  -T 3600 -D a=15000000000 -D ofst=2623948 -P300 >> res_inserts_2.txt &

--pgbench -h localhost -p 5432 -U postgres -f pgbench/payments-pgbench-datagen.sql payment  -r    -t 10  -D dttmtz='2021-10-01 00:00:00+00' -D intms=7.8 -D a=1

-- docker run -v /Users/sy/projects/payment-table-archival/pgbench:/pgbench  --env PGPASSWORD=postgres postgres:13    pgbench -h 192.168.1.76 -p 5432 -U postgres -f pgbench/payments-benchmark-noPk-seq.sql payment -r  -j 8 -c 8  -t 11425 -s 11425  -D a=0


