\set tmpid random(1,91424) 
BEGIN;
-- pick a random row - r1
-- csv in value in table , 
-- select r1 from the csv-table where id = <random-number> ( 0- 91k) // disk-read

  SELECT *   FROM public.tmp_pgbench_data where tmpid=:tmpid \gset r1_
  

	SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel  
	FROM public.payments WHERE id = :r1_id and createdat=':r1_createdat';
	 
    SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel  
    FROM public.payments WHERE paymentrequestid = ':r1_preqid' AND paymentrequestfrom = ':r1_preqfrm' and createdat=':r1_createdat';
     
     
      SELECT id, paymentrequestid, paymentrequestfrom, paymentid, paymentauthorizationsignatory, state, statetransitions, attributes, requestchanneltype, paymentrequest, paymentauthorization, payer, payee, coupondebitauths, createdat, transactionreceipt, authorizationreceipt, capturerequest, capturereceipt, paymentreceipt, transactioncode, ifi, isfinished, reversalreceipt, reversalrequest, reversaltransactionreceipt, resourceId, paymentCode, channelCode, paymentplan, get_channel(payments.id) AS paymentChannel 
       FROM payments WHERE paymentid = ':r1_pid' AND paymentauthorizationsignatory = ':r1_pauthsign' and createdat=':r1_createdat';
     

END;